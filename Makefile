.PHONY : zstory gstory zplay gplay quixe clean all test


MAIN    = src/main.inf
SRC     = src/*.inf
INCDIRS = .,./ext/,/usr/share/inform/6.31/module/,/usr/share/inform/6.31/include/
ZOPTS   = -S -X +include_path=$(INCDIRS)
GOPTS   = -S -G -E1 +include_path=$(INCDIRS)
ZFILE   = output.z5
GFILE   = output.ulx
QXFILE  = Game.gblorb.js
OUTDIR  = public
BROWSER ?= google-chrome
PYTHON  = python3
DEBUG   ?= 0

ifeq ($(DEBUG), 1)
	ZOPTS +=-D
	GOPTS +=-D
endif

all: gstory quixe

zstory : $(ZFILE)
gstory : $(GFILE)

zplay : zstory
	frotz $(ZFILE)

quixe : $(GFILE)
	cp -a quixe/media $(OUTDIR)/;
	cp -a quixe/lib $(OUTDIR)/;
	cp -a resources $(OUTDIR)/;
	cp -a src/index.html $(OUTDIR)/;
	$(PYTHON) quixe/tools/game2js.py --giload $(GFILE) > $(OUTDIR)/$(QXFILE)

gplay : quixe
	$(BROWSER) $(OUTDIR)/index.html

clean :
	rm -f $(ZFILE) $(GFILE);
	rm -rf $(OUTDIR)/*

$(ZFILE) : $(SRC)
	inform $(ZOPTS) $(MAIN) $(ZFILE)

$(GFILE) : $(SRC)
	inform $(GOPTS) $(MAIN) $(GFILE)

test : $(GFILE)
	TERM=xterm $(PYTHON) -m pytest tests $(TESTARGS)
