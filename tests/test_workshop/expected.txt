Start of a transcript of

Droop's Christmas Adventure
A text-based adventure game
Release 1 / Serial number 201912 / Inform v6.31 Library 6/11 SD
Interpreter version 0.5.4 / VM 3.1.2 / Library serial number 040227

>random
[Random number generator now predictable.]

>look

Workshop
You are in a dingy workshop. Sullen workers in garish garb huddle over a long workbench, they appear to be making toys. There is a door to the north. You are slumped at the end of the worktable. 
A cruel looking bugbear stalks the room, keeping a close eye on the workers.

A fellow goblin is working next to you.

On the table is a pot of paint.

In the corner is a large sack.

>look at workers
They all look pretty miserable, despite their colourful uniforms.

>look at goblin
You assume this Goblin is a he (even other Goblins struggle to tell sometimes). This one looks thoroughly miserable, and is currently focused on assembling a wooden doll.
>look at bugbear
he looks like means business

>look at doll
It's a wooden doll of a green dragon.

>look at paint
A pot of red paint. On closer inspection it seems to be an old ale mug.

>take paint
Taken.

>smell paint
It smells like your Auntie Gawp's famous toenail wine. She never did get her eyesight back after the last batch...

>kiss goblin
No-one wants to kiss a Goblin, even other Goblins!

>kiss workers
No-one wants to kiss a Goblin.

>kiss bigbear
You can't see any such thing.

>kiss bugbear
That is a REALLY bad idea...

>attack goblin
Even if you wanted to, if you openly attack the Goblin the Bugbear is sure to punish you.

>open door
If you try to leave the table while the Bugbear is around, he'll surely beat you to a bloody pulp.

>open sack
If you try to leave the table while the Bugbear is around, he'll surely beat you to a bloody pulp.

>n
If you try to leave the table while the Bugbear is around, he'll surely beat you to a bloody pulp.

>tell goblin to hide
The goblin doesn't look like it wants to chat.

>tell workers to hide
The workers don't look like they want to chat.

>tell bugbear to hide
The Bugbear doesn't look like he wants to chat.

>give paint to goblin

Without looking up, the Goblin takes the pot... and takes a large gulp of paint! A moment later he collapses onto the floor. With the sigh, the bugbear lumbers over and drags the unconscious goblin towards the door.

'YOU LOT BETTER KEEP YOUR HEADS DOWN TILL I GET BACK WITH A REPLACEMENT' he growls, as he locks the door behind him.

>look

Workshop
You are in a dingy workshop. Sullen workers in garish garb huddle over a long workbench, they appear to be making toys. There is a door to the north.
In the corner is a large sack.

>open door
It's locked.

>push door
It is fixed in place.

>hit door
Violence isn't the answer to this one.

>open sack
You open the sack, revealing a lockpick, a d20 die, a red book, a green book, a blue book, a coupon and some coal.

>look at coal
lumps of coal with little labels attached to them. How peculiar...

>take coal
I don't have a use for coal.

>inv
You are carrying:
  a note
  a uniform (being worn)

>read note
To pay his gambling debts (and substantial bar tab), this goblin has been sold into indentured servitude to Zanta Korp. Please keep this receipt as proof of purchase. Terms and conditions apply.

>look at uniform
It's a colourful elf costume. The ones from Lapland... NOT the noble wood elves!

>take lockpick
Taken.

>look at pick
A finely crafted lock-pickers kit. The label attached reads 'TO ASH'.

>unlock door with pick
Using the lockpick you manage to unlock the door.

>quit
Are you sure you want to quit? y
