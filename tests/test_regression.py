"""
inform regression tests via py.test
"""

import os
import pytest
import difflib
import subprocess

INTERPRETTER = 'glulxe'
TEST_INPUT = 'commands.txt'
TEST_OUTPUT = 'script.txt'
EXPECTED_OUTPUT = 'expected.txt'


@pytest.fixture
def game_file():
    basedir = os.path.dirname(__file__)
    game_file = os.path.abspath(os.path.join(basedir, '..', 'output.ulx'))
    return game_file


def pytest_generate_tests(metafunc):
    basedir = os.path.dirname(__file__)
    test_dirs = [
        os.path.relpath(os.path.join(basedir, d)) for d in os.listdir(basedir)
        if os.path.isdir(os.path.join(basedir, d)) and d.startswith('test')
    ]
    metafunc.parametrize("test_dir", test_dirs)


def test_inform(game_file, rebase, test_dir):
    os.chdir(test_dir)
    try:
        os.remove(TEST_OUTPUT)  # remove old test output
    except OSError:
        pass

    proc = subprocess.Popen(
        [INTERPRETTER, game_file],
        stdin=subprocess.PIPE, stdout=subprocess.PIPE,
    )
    cmds = str('\n'.join(['', 'replay', '', '', '', ''])).encode()

    try:
        proc.communicate(cmds, timeout=5)
    finally:
        proc.kill()

    with open(TEST_OUTPUT, 'r') as f:
        output = f.readlines()

    if rebase:
        with open(EXPECTED_OUTPUT, 'w') as f:
            f.writelines(output)
    else:
        with open(EXPECTED_OUTPUT, 'r') as f:
            expected_output = f.readlines()

        if output != expected_output:
            diff = list(difflib.unified_diff(
                expected_output, output,
                fromfile='{}/{}'.format(test_dir, EXPECTED_OUTPUT),
                tofile='{}/{}'.format(test_dir, TEST_OUTPUT),
            ))
            raise AssertionError('\n' + ''.join(diff))
