import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--rebase",
        action="store_true",
        default=False,
        help="Rebase tests",
    )


@pytest.fixture
def rebase(pytestconfig):
    return pytestconfig.getoption('rebase', False)
