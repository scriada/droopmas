# Droop's Christmas Adventure

 - wake up after a drunken night out chained to a seat in a workshop
 - have sold to a sweat shop for making presents for the evil overlord Zanta

# NPCs

 - Foreman (bugbear) in the workshop
 - Reanimated snowman, guarding the way out
 - Other slaves
 - Goblin next to you


# Rooms

```
          Outside
            |
  Workshop --- Stables --- Woodshed
            |
        Boiler room
```

 - workshop (you, foreman, other slaves)
 - stables (reindeer)
 - boiler room (boiler)
 - outside (blizzard so can't go alone)


# Items

 - boiler: need to put more wood in melt snowman.
 - wood (got wood joke?)
 - carrot and lumps of coal
 - reindeer (can be ridden once tamed)
 - 

# Challenges

 - get foreman to leave by making one of the other goblins sick (has to get a "spare" from the cells).
 - lead paint, surgical spirit, 
 - get rid of the foreman who won't let you out of the room
 - reanimated snowman guard (melt him to get the carrot nose) - what is he guarding
 - can melt by increasing the boiler temperature, or diverting the heating into the
 - snowman won't let you into the boiler room unless you are carrying wood
 - give carrot to reindeer to let it ride you out of here.



Better not go back in there, the foreman will see me.

If I go out into that blizzard unaided I'll freeze to death.

