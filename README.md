What is this?
=============

This is a small piece of interactive fiction using [Inform 6](https://en.wikipedia.org/wiki/Inform#Inform_6), that can be run from a browser.

Requirements
============

 * [inform6](https://en.wikipedia.org/wiki/Inform#Inform_6) installed on your machine
 * A browser with Javascript enabled
 * [glulxe](https://github.com/erkyrath/glulxe) or [frotz](https://www.ifarchive.org/indexes/if-archiveXinfocomXinterpretersXfrotz.html) interpretter for playing from the terminal and running tests (optional)
 * [quixe](https://github.com/erkyrath/quixe) (already included as a submodule)
 * Python

Alternatively, all of these are available in the Docker image [scriada/if](https://hub.docker.com/r/scriada/if) that is used by `.gitlab-ci.yml`
for CI/CD.

Compiling and Running
=====================

To compile:
```sh
make gstory
```

To run:
```sh
make gplay
```

To compile in debug mode use `make DEBUG=1 make ...`

See `Makefile` for details.

Testing
=======

You will need the [glulxe](https://github.com/erkyrath/glulxe) interpretter installed to run the tests.

Run with `make test` or `py.test tests`.

Each regression test has it's own folder in `tests/`. In contains the inputs and expected outputs,
recorded using the `RECORDING ON` and `SCRIPT ON` inform macros.

You can rebase the tests with `py.test tests --rebase` or `make test TESTARGS=--rebase`.

Note: some tests make use of the debugging verbs in Inform that are only available when the game is
compiled in debug mode.

 
Layout
======

```
public/      # generated output
ext/         # language extensions
quixe/       # quixe submodule
resources/   # game resources
src/         # game source code
Makefile     # 
README.md    # this file
```

Resources
=========

 * [Inform Designers Manual](https://www.inform-fiction.org/manual/html/contents.html)
 * [Inform 6 compiler](https://launchpad.net/ubuntu/+source/inform6-compiler)
 * [Extensions](https://ifarchive.org/indexes/if-archiveXinfocomXcompilersXinform6XlibraryXcontributions.html)
 * [Example stories](https://ifarchive.org/indexes/if-archiveXinfocomXcompilersXinform6Xexamples.html)
